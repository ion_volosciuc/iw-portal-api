const cookieParser = require('cookie-parser');
const {User} = require('../models/user');

module.exports = (req, res, next) => {
    const token = req.cookies.sessionID;
    const cookies = req.cookies.sessionID;

    console.log(JSON.stringify(cookies));
    User.findByToken(token).then(
        user => {
            if (!user) {
                return Promise.reject();
            }

            req.user = user;
            req.token = token;
            next();
        }).catch(err => res.status(401).send())
};