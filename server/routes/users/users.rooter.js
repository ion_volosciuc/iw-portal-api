const {User} = require('../../models/user');
const router = require('express').Router();
const {ObjectID} = require('mongodb');
const authenticate = require('../../middleware/authentificate');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

router.get('/', (req, res) => {
    User.find().then(
        users => {
            res.send(users);
        }, err => {
            res.status(400).send(err);
        }
    )
});

router.get('/:id', (req, res) => {
    const id = req.params.id;
    User.findById(id).then(
        user => {
            if (user)
                res.send(user);
            else
                res.status(404).send()
        }, err => {
            res.status(400).send(err);
        }
    )
});

router.get('/team/:team', (req, res) => {
    const team = req.params.team;
    User.find({team: new RegExp('\\b' + team + '\\b', 'i')}).then(
        doc => res.send(doc),
        err => res.status(400).send(err)
    );
});

router.post('/add_user', (req, res) => {
    const user = new User(req.body);
    user.save().then(
        doc => res.send(doc),
        err => res.status(400).send(err)
    )
});

router.post('/login', (req, res) => {
    const user = req.body;

    User.findByCredentials(user.email, user.password).then(user => {
        user.generateAuthToken().then(token => {
            res.cookie('sessionID', token, {httpOnly: true/*, secure: true*/}).send(user.toJSON());
        });
    }).catch(err => {
        res.status(400).send(err);
    })
});

// TMP
router.post('/logout', authenticate, (req, res) => {
    const user = req.user;

    user.removeToken(req.token).then(
        () => res.status(200).send('OK'),
        () => res.status(400).send('OK')
    );
    // res.send(user);
    // User.findOne(user._id).then(user => {
    //     user.tokens = [];
    // })
});

router.post('/:id', (req, res) => {
    const id = req.params.id;
    const userFields = _.pick(req.body, ['lname', 'fname', 'email', 'team', 'hired', 'position', 'photo', 'phone', 'dateOfBirth', 'hiredDate', 'vacantionDays', 'description', 'hobby', 'socialContacts']);
    userFields.updatedAt = new Date();

    User.findByIdAndUpdate(id, userFields).then(
        user => {
            if (!user)
                res.status(404).send();
            else
                res.status(200).send('OK');
        },
        err => res.status(400).send(err)
    );
});

router.delete('/:id', (req, res) => {
    const id = req.params.id;
    User.findByIdAndRemove(id).then(
        obj => {
            if (obj)
                res.send('OK');
            else
                res.status(404).send();
        },
        err => res.status(400).send()
    )
});

module.exports = router;