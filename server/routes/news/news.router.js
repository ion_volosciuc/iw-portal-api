const {News} = require('../../models/news');
const router = require('express').Router();
const {ObjectID} = require('mongodb');
const _ = require('lodash');
const cors = require('cors')

router.get('/', cors(), (req, res) => {
    News.find().then(
        news => {
            res.send({news});
        }, err => {
            res.status(400).send(err);
        }
    )
});

router.get('/:id', (req, res) => {
    const id = req.params.id;
    if (!ObjectID.isValid(id)) {
        return res.status(404).send()
    }
    News.findById(id).then(
        news => {
            if (news) {
                return res.send({news})
            } else {
                return res.status(404).send()
            }
        },
        err => res.status(400).send(err.message)
    )
});

router.post('/', (req, res) => {
    let news = new News(req.body);
    news.updatedAt = new Date();

    news.save().then((doc) => {
        res.send(doc);
    }, (err) => {
        res.status(400).send(err);
    })
});

router.post('/update/:id', (req, res) => {
    const id = req.params.id;
    let body = _.pick(req.body, ['title', 'releaseDate', 'description']);
    body.updatedAt = new Date();

    News.findByIdAndUpdate(id, {$set: body}, {new: true}).then(
        news => {
            if (!news) return res.status(404).send();
            return res.send('OK');
        },
        err => res.status(400).send()
    )
});

router.delete('/:id', (req, res) => {
    const id = req.params.id;

    News.findByIdAndRemove(id).then(
        obj => {
            if (!obj)
                res.status(404).send();
            else
                res.send('OK');
        },
        err => res.status(400).send()
    )
});

module.exports = router;


