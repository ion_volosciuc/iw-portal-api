const mongoose = require('mongoose');

const News = mongoose.model('News', {
    title: {
        type: String,
        required: true,
        minLength: 4
    },
    author: {
        type: String,
        required: true,
        minLength: 6
    },
    releaseDate: {
        type: String,
        requiredL: true
    },
    icon: {
        type: Object,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        required: true
    }
});

module.exports = {News};