const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const _ = require('lodash');

const UserSchema = new mongoose.Schema({
    lname: {
        type: String,
        required: true,
        minLength: 2,
        maxLength: 35
    },
    fname: {
        type: String,
        required: true,
        minLength: 2,
        maxLength: 35
    },
    email: {
        type: String,
        required: true,
        maxLength: 254
    },
    password: {
        type: String,
        required: true
    },
    team: {
        type: String,
        required: true
    },
    hiredDate: {
        type: String
    },
    position: {
        type: String
    },
    gender: {
        type: String
    },
    photo: {
        type: String
    },
    city: {
        type: String,
        maxLength: 35
    },
    phone: {
        type: String,
        maxLength: 15
    },
    dateOfBirth: {
        type: String
    },
    vacantionDays: {
        type: Number
    },
    description: {
        type: String
    },
    hobby: {
        type: Array
    },
    registeredAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date
    },
    socialContacts: {
        type: Object
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
});

UserSchema.methods.toJSON = function() {
    const user = this;
    return _.pick(user, ['_id', 'email', 'fname', 'lname', 'team', 'photo', 'position', 'phone', 'description'])
};

UserSchema.methods.generateAuthToken = function() {
    const user = this;
    const access = 'auth';
    const token = jwt.sign({_id: user._id.toHexString(), access}, 'salt').toString();

    user.tokens.push({access, 'token': token});
    return user.save().then(() => token)
};

UserSchema.methods.removeToken = function(token) {
    const user = this;

    return user.update({
        $pull: {
            tokens: {
                token: token
            }
        }
    });
};

UserSchema.statics.findByCredentials = function(email, password) {
    const User = this;

    return User.findOne({email}).then(user => {
        if (!user) {
            return Promise.reject();
        }

        return new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err, res) => {
                if (res) {
                    resolve(user);
                } else {
                    reject();
                }
            })
        })
    })
};

UserSchema.statics.findByToken = function(token) {
    const user = this;
    let decoded;

    try {
        decoded = jwt.verify(token, 'salt');
    } catch(e) {
        return Promise.reject();
    }

    return User.findOne({
        '_id': decoded._id,
        'tokens.access': 'auth',
        'tokens.token': token
    })
};

UserSchema.pre('save', function(next) {
    const user = this;

    if (user.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
                user.password = hash;
                next();
            })
        })
    } else {
        next();
    }
});

const User = mongoose.model('User', UserSchema);


module.exports = {User};