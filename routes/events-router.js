const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.send([
        {
            title: 'Test',
            date: '2018-10-08'
        },
        {
            title: 'Test2',
            date: '2018-10-09'
        }
    ])
});

router.get('/:eventId', (req, res) => {
    res.send([
        {
            id: req.params.eventId,
            title: 'Test',
            date: '2018-10-08'
        },
    ])
});

module.exports = router;