const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.send([
        {
            title: 'notification1',
            date: '2018-10-08'
        },
        {
            title: 'notification2',
            date: '2018-10-09'
        }
    ])
})

router.get('/:notoficationId', (req, res) => {
    res.send([
        {
            title: req.params.notificationId,
            date: '2018-10-08'
        }
    ])
})